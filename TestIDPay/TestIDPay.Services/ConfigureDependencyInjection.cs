﻿using Microsoft.Extensions.DependencyInjection;

namespace TestIDPay.Services
{
    public static class ConfigureDependencyInjection
    {
        public static IServiceCollection ConfigureTestIDPayDependencyInjection(this IServiceCollection services)
        {

            services.AddScoped<IDemografiService, DemografiService>();
            services.AddScoped<IPhotoIDService, PhotoIDService>();
            return services;

        }

    }
}