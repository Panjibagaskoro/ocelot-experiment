﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestIDPay.Data.Entity;
using TestIDPay.Model;
using TestIDPay.Model.Common;
using Microsoft.EntityFrameworkCore;

namespace TestIDPay.Services
{
    public interface IPhotoIDService
    {         
        Task<BaseResponse> Get(int id);
        BaseResponse Create(PhotoIDModel model);
        BaseResponse Update(PhotoIDModel model,int id);
        BaseResponse Delete(int id);
    }
    public class PhotoIDService : IPhotoIDService
    {
        private readonly TestIDPayContext db;

        public PhotoIDService(TestIDPayContext ctx)
        {
            db = ctx;
        }

        public async Task<BaseResponse> Get(int id)
        {
            BaseResponse response = ResponseConstant.ERROR;
            try
            {
                var lst = await (from a in db.PhotoID
                                 where a.Id == id
                                 select new PhotoIDViewModel
                                 {
                                     id = a.Id,
                                     nik = a.NIK,
                                     path = a.PathPhoto
                                 }
                                 ).FirstOrDefaultAsync();
                if(lst!=null)
                {
                    response = ResponseConstant.OK;
                    response.data=lst;
                }
                else
                {
                    response = ResponseConstant.NOT_FOUND;
                }
            }
            catch(Exception e)
            {
                response.message = e.Message;
            }
            return response;
        }

        public BaseResponse Create(PhotoIDModel model)
        {
            BaseResponse response=ResponseConstant.ERROR;
            using(var dbcxtransaction=db.Database.BeginTransaction())
            {
                try
                {
                    var check = db.PhotoID.Where(a => a.NIK == model.nik).FirstOrDefault();
                    if(check==null)
                    {
                        check = new PhotoID
                        {
                            NIK = model.nik,
                            PathPhoto = model.path
                        };
                        db.PhotoID.Add(check);
                        db.SaveChanges();
                        dbcxtransaction.Commit();
                        response = ResponseConstant.OK;
                    }
                    else
                    {
                        dbcxtransaction.Rollback();
                        response = ResponseConstant.DUPLICATE_ENTRY;
                    }
                }
                catch(Exception e)
                {
                    dbcxtransaction.Rollback();
                    response.message = e.Message;
                }
            }
            return response;
        }

        public BaseResponse Update(PhotoIDModel model, int id)
        {
            BaseResponse response = ResponseConstant.ERROR;
            using (var dbcxtransaction = db.Database.BeginTransaction())
            {
                try
                {
                    var check = db.PhotoID.Where(a => a.NIK == model.nik && a.Id==id).FirstOrDefault();
                    if (check != null)
                    {

                        check.NIK = model.nik;
                        check.PathPhoto = model.path;


                        db.Entry(check).State=EntityState.Modified;
                        db.SaveChanges();
                        dbcxtransaction.Commit();
                        response = ResponseConstant.OK;
                    }
                    else
                    {
                        dbcxtransaction.Rollback();
                        response = ResponseConstant.DUPLICATE_ENTRY;
                    }
                }
                catch (Exception e)
                {
                    dbcxtransaction.Rollback();
                    response.message = e.Message;
                }
            }
            return response;
        }

        public BaseResponse Delete(int id)
        {
            BaseResponse response=ResponseConstant.ERROR;
            using(var dbcxtransaction = db.Database.BeginTransaction())
            {
                try
                {
                    var check=db.PhotoID.Where(a => a.Id == id).FirstOrDefault();
                    if(check!=null)
                    {
                        db.Remove(check);
                        db.SaveChanges();
                        dbcxtransaction.Commit();
                        response=ResponseConstant.OK;
                    }
                    else
                    {
                        response = ResponseConstant.NOT_FOUND;
                    }

                }
                catch(Exception e)
                {
                    dbcxtransaction.Rollback();
                    response.message = e.Message;
                
                }
            }
            return response;
        }
    }
}
