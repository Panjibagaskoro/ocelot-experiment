﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestIDPay.Data.Entity;
using TestIDPay.Model;
using TestIDPay.Model.Common;

namespace TestIDPay.Services
{
    public interface IDemografiService
    {
        Task<BaseResponse> Get();
        Task<BaseResponse> Get(int id);
        BaseResponse Create(DemografiModel model);
        BaseResponse Update(DemografiModel model,int id);
        BaseResponse Delete(int id);
    }
    public class DemografiService:IDemografiService
    {
        private readonly TestIDPayContext db;

        public DemografiService(TestIDPayContext ctx)
        {
            db = ctx;
        }

        public async Task<BaseResponse> Get()
        {
            BaseResponse response = ResponseConstant.ERROR;
            try
            {
                var lst = await (from a in db.Demografi
                                 select new DemografiViewModel
                                 {
                                     id = a.Id,
                                     agama = a.Agama,
                                     alamat = a.Alamat,
                                     desa = a.Desa,
                                     golongan_darah = a.GolonganDarah,
                                     jenis_kelamin = a.JenisKelamin,
                                     kecamatan = a.Kecamatan,
                                     kelurahan = a.Kelurahan,
                                     kewarganegaraan = a.Kewarganegaraan,
                                     kode_pos = a.KodePos,
                                     kota = a.Kota,
                                     masa_berlaku = a.Masa_Berlaku,
                                     nama = a.Nama,
                                     nik = a.NIK,
                                     pekerjaan = a.Pekerjaan,
                                     provinsi = a.Provinsi,
                                     rt = a.RT,
                                     rw = a.RW,
                                     status_perkawinan = a.StatusPerkawinan,
                                     tempat_lahir = a.TempatLahir
                                 }
                               ).ToListAsync();
                if(lst.Count>0)
                {
                    response = ResponseConstant.OK;
                    response.data = lst;
                }
            }
            catch(Exception e)
            {
                response.message = e.Message;
            }
            return response;
        }

        public async Task<BaseResponse> Get(int id)
        {
            BaseResponse response = ResponseConstant.ERROR;
            try
            {
                var lst = await (from a in db.Demografi
                                 where a.Id==id
                                 select new DemografiViewModel
                                 {
                                     id = a.Id,
                                     agama = a.Agama,
                                     alamat = a.Alamat,
                                     desa = a.Desa,
                                     golongan_darah = a.GolonganDarah,
                                     jenis_kelamin = a.JenisKelamin,
                                     kecamatan = a.Kecamatan,
                                     kelurahan = a.Kelurahan,
                                     kewarganegaraan = a.Kewarganegaraan,
                                     kode_pos = a.KodePos,
                                     kota = a.Kota,
                                     masa_berlaku = a.Masa_Berlaku,
                                     nama = a.Nama,
                                     nik = a.NIK,
                                     pekerjaan = a.Pekerjaan,
                                     provinsi = a.Provinsi,
                                     rt = a.RT,
                                     rw = a.RW,
                                     status_perkawinan = a.StatusPerkawinan,
                                     tempat_lahir = a.TempatLahir,
                                     photo=(from pt in db.PhotoID
                                            where pt.NIK==a.NIK
                                            select new PhotoIDViewModel
                                            {
                                                id=pt.Id,
                                                nik=pt.NIK,
                                                path=pt.PathPhoto
                                            }
                                            ).FirstOrDefault()
                                 }
                               ).FirstOrDefaultAsync();
                if (lst!=null)
                {
                    response = ResponseConstant.OK;
                    response.data = lst;
                }
                else
                {
                    response = ResponseConstant.NO_RESULT;
                }
            }
            catch (Exception e)
            {
                response.message = e.Message;
            }
            return response;
        }

        public BaseResponse Create(DemografiModel model)
        {
            BaseResponse response = ResponseConstant.ERROR;
            using (var dbcxtransaction = db.Database.BeginTransaction())
            {
                try
                {
                    var check = db.Demografi.Where(a => a.NIK == model.nik).FirstOrDefault();
                    if (check == null)
                    {
                        check = new Demografi
                        {
                            Agama = model.agama,
                            Alamat = model.alamat,
                            Desa = model.desa,
                            Masa_Berlaku = model.masa_berlaku,
                            GolonganDarah = model.golongan_darah,
                            JenisKelamin = model.jenis_kelamin,
                            Kecamatan = model.kecamatan,
                            Kelurahan = model.kelurahan,
                            Kewarganegaraan = model.kewarganegaraan,
                            KodePos = model.kode_pos,
                            Kota = model.kota,
                            Nama = model.nama,
                            NIK = model.nik,
                            Pekerjaan = model.pekerjaan,
                            Provinsi = model.provinsi,
                            RT = model.rt,
                            RW = model.rw,
                            StatusPerkawinan = model.status_perkawinan,
                            TempatLahir = model.tempat_lahir
                        };
                        db.Demografi.Add(check);
                        db.SaveChanges();
                        dbcxtransaction.Commit();
                        response = ResponseConstant.OK;
                        response.data = check.Id;
                    }
                    else
                    {
                        response = ResponseConstant.DUPLICATE_ENTRY;
                    }
                }
                catch (Exception e)
                {
                    dbcxtransaction.Rollback();
                    response.message = e.Message;
                }
            }
            return response;
        }
        public BaseResponse Update(DemografiModel model, int id)
        {
            BaseResponse response = ResponseConstant.ERROR;
            using (var dbcxtransaction = db.Database.BeginTransaction())
            {
                try
                {
                    var check = db.Demografi.Where(a => a.NIK == model.nik && a.Id==id).FirstOrDefault();
                    if (check != null)
                    {
                        check.Agama = model.agama;
                        check.Alamat = model.alamat;
                        check.Desa = model.desa;
                        check.Masa_Berlaku = model.masa_berlaku;
                        check.GolonganDarah = model.golongan_darah;
                        check.JenisKelamin = model.jenis_kelamin;
                        check.Kecamatan = model.kecamatan;
                        check.Kelurahan = model.kelurahan;
                        check.Kewarganegaraan = model.kewarganegaraan;
                        check.KodePos = model.kode_pos;
                        check.Kota = model.kota;
                        check.Nama = model.nama;
                        check.NIK = model.nik;
                        check.Pekerjaan = model.pekerjaan;
                        check.Provinsi = model.provinsi;
                        check.RT = model.rt;
                        check.RW = model.rw;
                        check.StatusPerkawinan = model.status_perkawinan;
                        check.TempatLahir = model.tempat_lahir;
                        db.Entry(check).State = EntityState.Modified;
                        db.SaveChanges();
                        dbcxtransaction.Commit();
                        response = ResponseConstant.OK;
                        response.data = check.Id;
                    }
                    else
                    {
                        response = ResponseConstant.NOT_FOUND;
                    }
                }
                catch (Exception e)
                {
                    dbcxtransaction.Rollback();
                    response.message = e.Message;
                }
            }
            return response;
        }

        public BaseResponse Delete(int id)
        {
            BaseResponse response = ResponseConstant.ERROR;
            using(var dbcxtransaction=db.Database.BeginTransaction())
            {
                try
                {
                    var getData = db.Demografi.Where(a => a.Id == id).FirstOrDefault();
                    if (getData != null)
                    {
                        var photoid = db.PhotoID.Where(a => a.NIK == getData.NIK).ToList();
                        if (photoid.Count > 0)
                        {
                            db.RemoveRange(photoid);
                            db.SaveChanges();
                        }

                        db.Remove(getData);
                        db.SaveChanges();
                        
                        dbcxtransaction.Commit();
                        response = ResponseConstant.OK;
                    }
                    else
                    {
                        dbcxtransaction.Rollback();
                        response.message = "Failed to delete data";
                    }
                }
                catch(Exception e)
                {
                    dbcxtransaction.Rollback();
                    response.message = e.Message;
                }
            }
            return response;
        }
    }
}
