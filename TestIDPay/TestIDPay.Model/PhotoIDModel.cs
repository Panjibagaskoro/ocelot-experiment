﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestIDPay.Model
{
    public class PhotoIDModel
    {
        public string nik { get; set; }
        public string path { get; set; }
    }

    public class PhotoIDViewModel : PhotoIDModel
    {
        public int id { get; set; }
    }
}
