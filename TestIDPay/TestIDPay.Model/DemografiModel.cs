﻿using System.ComponentModel.DataAnnotations;

namespace TestIDPay.Model
{
    public class DemografiModel
    {
		[Required, StringLength(16)]
		public string nik { get; set; }
		[Required, StringLength(255)]
		public string nama { get; set; }
		[Required, StringLength(255)]
		public string tempat_lahir { get; set; }
		[Required, StringLength(40)]
		public string jenis_kelamin { get; set; }
		[Required, StringLength(5)]
		public string golongan_darah { get; set; }
		[Required, StringLength(255)]
		public string alamat { get; set; }
		[Required, StringLength(10)]
		public string rt { get; set; }
		[Required, StringLength(10)]
		public string rw { get; set; }
		[Required, StringLength(100)]
		public string kelurahan { get; set; }
		[Required, StringLength(100)]
		public string desa { get; set; }
		[Required, StringLength(100)]
		public string kecamatan { get; set; }
		[Required, StringLength(100)]
		public string kota { get; set; }
		[Required, StringLength(255)]
		public string provinsi { get; set; }
		[Required, StringLength(40)]
		public string agama { get; set; }
		[Required, StringLength(10)]
		public string kode_pos { get; set; }
		[Required, StringLength(100)]
		public string status_perkawinan { get; set; }
		[StringLength(100)]
		public string pekerjaan { get; set; }
		[StringLength(100)]
		public string kewarganegaraan { get; set; }
		public DateTime? masa_berlaku { get; set; }

    }

    public class DemografiViewModel : DemografiModel
    { 
		public int id { get; set; }
		public PhotoIDViewModel photo { get; set; }
	
	}

}