﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestIDPay.Model;
using TestIDPay.Model.Common;
using TestIDPay.Services;

namespace TestIDPay.PhotoID.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class PhotoIDController : ControllerBase
    {
        private readonly IPhotoIDService _service;
        public PhotoIDController(IPhotoIDService service)
        {
            _service = service;
        }

        [HttpGet("{id}")]
        public async Task<BaseResponse>Get(int id)
        {
            BaseResponse response = await _service.Get(id);
            HttpContext.Response.StatusCode = response.code;
            return response;
        }

        [HttpPost]
        public BaseResponse Create([FromBody]PhotoIDModel model)
        {
            BaseResponse response = _service.Create(model);
            HttpContext.Response.StatusCode = response.code;
            return response;
        }

        [HttpPut("{id}")]
        public BaseResponse Update([FromBody] PhotoIDModel model,int id)
        {
            BaseResponse response = _service.Update(model,id);
            HttpContext.Response.StatusCode = response.code;
            return response;
        }

        [HttpDelete("{id}")]
        public BaseResponse Delete(int id)
        {
            BaseResponse response = _service.Delete(id);
            HttpContext.Response.StatusCode = response.code;
            return response;
        }
    }
}
