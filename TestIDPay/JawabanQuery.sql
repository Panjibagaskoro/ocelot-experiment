--Create Database testidpay

--use testidpay

--1
create table Demografi(
Id int IDENTITY(1,1) Primary key,
NIK varchar(16) not null,
[Nama] varchar(255) not null,
[TempatLahir] varchar(255) not null,
[JenisKelamin] varchar(40) not null,
[GolonganDarah] varchar(5) not null,
[Alamat] varchar(255) not null,
[RT] varchar(10) not null,
[RW] varchar(10) not null,
[Kelurahan] varchar(100) not null,
Desa varchar(100) not null,
Kecamatan varchar(100) not null,
Kota varchar(100) not null,
Provinsi varchar(255) not null,
Agama varchar(40) not null,
KodePos varchar(10) not null,
StatusPerkawinan varchar(100) not null,
Pekerjaan varchar(100),
Kewarganegaraan varchar(100),
Masa_Berlaku date
)

CREATE UNIQUE INDEX NIK
ON demografi(NIK)

create table PhotoID(
Id int identity(1,1) primary key,
NIK varchar(16) Not null  FOREIGN KEY REFERENCES Demografi(NIK),
PathPhoto varchar(255) not null
)

--2
DECLARE @i int = 1,@NIK varchar(16) =''
while @i<6
Begin


	set @NIK=Concat('317406080894000',@i)
	insert into Demografi(NIK,Nama,TempatLahir,JenisKelamin,GolonganDarah,Alamat,[RT],[RW],[Kelurahan],Desa,Kecamatan,Kota,Provinsi,Agama,KodePos,StatusPerkawinan,Pekerjaan,Kewarganegaraan,Masa_Berlaku)

	select @NIK,CONCAT('test',@i),'Jakarta',case when @i%2 = 0 then 'W' else 'P' end,'A','Testing Cilandak','05','08','Wakanda','Deplu','Cipete','Gotham','Metropolis Selatan','Kryptonian','14045','Kawin','Wirausaha','WNM','2045-01-01' 
	
	insert into PhotoID(NIK,PathPhoto)
	select @NIK,CONCAT('test',@i,'.png')
	
	set @i=@i+1
end

--3
Create procedure GetDemografiPhoto
as
select a.NIK,Nama,TempatLahir,JenisKelamin,GolonganDarah,Alamat,[RT],[RW],[Kelurahan],Desa,Kecamatan,Kota,Provinsi,Agama,KodePos,StatusPerkawinan,Pekerjaan,Kewarganegaraan,Masa_Berlaku,b.PathPhoto
from Demografi a
join PhotoID b on a.NIK=b.NIK
Go

exec GetDemografiPhoto


