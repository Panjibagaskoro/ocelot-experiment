using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using TestIDPay.Data.Entity;
using TestIDPay.Services;

var builder = WebApplication.CreateBuilder(args);
string connectionString = builder.Configuration.GetConnectionString("DefaultConnection");
// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle

builder.Services.AddCors(options =>
{
    options.AddDefaultPolicy(policy =>
    {
        policy.AllowAnyOrigin();
        policy.WithMethods(new string[4] { "GET", "POST", "PUT", "DELETE" });
        policy.AllowAnyHeader();
    });
});



builder.Services.AddEntityFrameworkSqlServer()
    .AddDbContextPool<TestIDPayContext>((serviceProvider, options) =>
           options.UseSqlServer(connectionString)
                  .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking)
                  .EnableDetailedErrors(true)
                  .UseInternalServiceProvider(serviceProvider)
                  .ConfigureWarnings(c => c.Log((RelationalEventId.CommandExecuting, LogLevel.Debug)))
                  );


builder.Services.ConfigureTestIDPayDependencyInjection();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
