﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestIDPay.Model;
using TestIDPay.Model.Common;
using TestIDPay.Services;

namespace TestIDPay.Demografi.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class DemografiController : ControllerBase
    {
        private readonly IDemografiService _service;
        public DemografiController(IDemografiService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<BaseResponse>Get()
        {
            BaseResponse response=await _service.Get();
            HttpContext.Response.StatusCode = response.code;
            return response;
        }

        [HttpGet("{id}")]
        public async Task<BaseResponse>Get(int id)
        {
            BaseResponse response=await _service.Get(id);
            HttpContext.Response.StatusCode=response.code;
            return response;
        }

        [HttpPost]
        public BaseResponse Create([FromBody]DemografiModel model)
        {
            BaseResponse response=_service.Create(model);
            HttpContext.Response.StatusCode = response.code;
            return response;
        }

        [HttpPut("{id}")]
        public BaseResponse Update([FromBody]DemografiModel model,int id)
        {
            BaseResponse response=_service.Update(model,id);
            HttpContext.Response.StatusCode = response.code;
            return response;
        }

        [HttpDelete("{id}")]
        public BaseResponse Delete(int id)
        {
            BaseResponse response= _service.Delete(id);
            HttpContext.Response.StatusCode=response.code;
            return response;
        }
    }
}
