﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestIDPay.Data.Entity
{
    public partial class TestIDPayContext : DbContext
    {
        public TestIDPayContext(DbContextOptions<TestIDPayContext> options)
        : base(options)
        {
        }

        public virtual DbSet<Demografi> Demografi { get; set; }
        public virtual DbSet<PhotoID> PhotoID { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Demografi>(entity =>
            {
                entity.HasKey(a => a.Id);
                entity.HasIndex(a => a.NIK);
            });
            modelBuilder.Entity<PhotoID>(entity =>
            {
                entity.HasOne(d => d.Demografi)
                .WithMany(p => p.PhotoID)
                .HasPrincipalKey(a => a.NIK)
                .HasForeignKey(d => d.NIK)
                .OnDelete(DeleteBehavior.NoAction);
            });
        }
    }
}
