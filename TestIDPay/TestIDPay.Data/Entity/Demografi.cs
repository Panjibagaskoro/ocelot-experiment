﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestIDPay.Data.Entity
{
	public partial class Demografi
	{
		public Demografi()
        {
			PhotoID =new HashSet<PhotoID>();

        }
		[Key]
		public int Id { get; set; }

		[Required, StringLength(16)]
		public string NIK { get; set; }
		[Required, StringLength(255)]
		public string Nama { get; set; }
		[Required, StringLength(255)]
		public string TempatLahir { get; set; }
		[Required, StringLength(40)]
		public string JenisKelamin { get; set; }
		[Required, StringLength(5)]
		public string GolonganDarah { get; set; }
		[Required, StringLength(255)]
		public string Alamat { get; set; }
		[Required, StringLength(10)]
		public string RT { get; set; }
		[Required, StringLength(10)]
		public string RW { get; set; }
		[Required, StringLength(100)]
		public string Kelurahan { get; set; }
		[Required, StringLength(100)]
		public string Desa { get; set; }
		[Required, StringLength(100)]
		public string Kecamatan { get; set; }
		[Required, StringLength(100)]
		public string Kota { get; set; }
		[Required, StringLength(255)]
		public string Provinsi { get; set; }
		[Required, StringLength(40)]
		public string Agama { get; set; }
		[Required, StringLength(10)]
		public string KodePos { get; set; }
		[Required, StringLength(100)]
		public string StatusPerkawinan { get; set; }
		[StringLength(100)]
		public string Pekerjaan { get; set; }
		[StringLength(100)]
		public string Kewarganegaraan {get;set; }
		public DateTime? Masa_Berlaku { get; set; }
		[InverseProperty("Demografi")]
		public virtual ICollection<PhotoID> PhotoID { get; set; }
	}
}
