﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestIDPay.Data.Entity
{
    public partial class PhotoID
    {
        [Key]
        public int Id { get; set; }
        [Required,StringLength(16)]
        public string NIK { get; set; }
        [Required,StringLength(255)]
        public string PathPhoto { get; set; }
        [ForeignKey(nameof(NIK))]
        [InverseProperty("PhotoID")]
        public virtual Demografi Demografi { get; set; }
    }
}
